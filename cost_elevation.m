function [ cost ] = cost_elevation(latitudes, longitudes)
%  Discrete Cost function using Google Maps API Elevation Data
%  Takes x, y coordinate vectors as inputs
%  Returns cost based on smoothness (High-pass filter / spatial gradient)
%  Copyright 2017 - Tony S. Zhang (TSZ)
    
    % get elevation data at locations
    ele = elevation(latitudes, longitudes);
    
    
    
    
    
    
    cost = ele;
end


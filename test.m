clear; clc; close all;

%% Terrain spatial gradient
v = -2:0.2:2;

[x, y] = meshgrid(v);
z = x .* exp(-x.^2 - y.^2);
[dx,dy] = gradient(z,.2,.2);
dxy = sqrt(dx.^2 + dy.^2); % gradient vector lengths

figure(1)

ax1 = subplot(1,3,1);
surfc(x,y,z)

ax2 = subplot(1,3,2);
contour(v,v,z)
hold on
quiver(v,v,dx,dy)
hold off

ax3 = subplot(1,3,3);
surfc(x,y,dxy)

hlink = linkprop([ax1,ax2,ax3],{'CameraPosition','CameraUpVector'});

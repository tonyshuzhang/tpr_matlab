function [elevation_out] = elevation(latitudes, longitudes)
%  Elevation function using Google Maps API
%  Takes [lat, lon] coordinates vectors as inputs
%  Returns elevation (in meters) above local mean sea level (LMSL)
%  Copyright 2017 - Tony S. Zhang (TSZ)
%  Google Dashboard: https://console.developers.google.com/apis/dashboard
    
    cutoff = 128; % Google API max limit = 512 locations/request
    % Consider URL length limits and HTTP request time-outs: 128 works well
    if length(latitudes) > cutoff % Recursive calls
        elevation_out = [elevation(latitudes(1:cutoff), longitudes(1:cutoff));...
                         elevation(latitudes(cutoff+1:end), longitudes(cutoff+1:end))];
    else
        % Please replace with your own Google Maps API key
        api_key_TSZ= 'AIzaSyDCYOkrWuU9QyHPjtqWN2aJw-wTkrFkKF0';
        key = ['&key=' api_key_TSZ];  % Use Tony's
        preamble = 'https://maps.googleapis.com/maps/api/elevation/json?locations=';

        locations = [num2str(latitudes(1)) ',' num2str(longitudes(1))]; 
        if length(latitudes) > 1 % If not single coordinate pair
            for i = 2:length(latitudes)
                locations = [locations '|' num2str(latitudes(i)) ',' num2str(longitudes(i))];
            end
        end
        url = [preamble locations key];
        display(url);
        data = webread(url); % Get JSON via RESTful API
        elevation_out = cat(1, data.results.elevation); % Structs to vector
    end
end


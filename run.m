%% Data inputs - Please provide all parameters
clear; clc; close all;

% Canada - Coordinates in [lat?, lon?]
Toronto = [43.6532, -79.3832];
Montreal = [45.5017, -73.5673];
Edmonton = [53.5444, -113.4909];
Calgary = [51.0486, -114.0708];
% Road show cities
Paris = [48.8566, 2.3522]; % June 20
Toulouse = [43.6047, 1.4442]; 
Milan = [45.4654, 9.1859]; % June 23
% Other EU cities
Berlin = [52.5200, 13.4050];
Madrid = [40.4168, -3.7038];
Brussels = [50.8503, 4.3517];
London = [51.5074, -0.1278];

% City pairs?
% Paris - Berlin
% Paris - Brussels
% Milan - Brussles
% Milan - Berlin
% Choose city pair here
city1 = Paris;
city2 = Toulouse;
% for 20% margin
margin = 0.2 * [abs(city1(1)-city2(1)), abs(city1(2)-city2(2))];
corner1 = city1 + margin; % needs manual change here
corner2 = city2 - margin;

% Resolution parameters - consider requirements
nLat = 300; % number of latitude grid points between 2 corners
nLon = 150; % number of longitude grid points between 2 corners
nMesh = 100; % [lat, lon] mesh for 3D plot interpolation, should be > nLat

%% 0. Set up search grid (rectangle w/ two cities at opposite corners)

[grid, lats, lons] = get_grid(corner1, corner2, nLat, nLon);

% Define criteria:
% i - elevation roughness (spatial gradient or HPF)
% ii - cost of land...
% iii - 
% iv - 

%% 1a.i Get Elevation Map (base) 

eles = elevation(lats, lons); % calls to Google Maps RESTful API

%%
% Convert from (x,y,z) vectors to surface mesh
xgv = linspace(corner1(2), corner2(2), nMesh);
ygv = linspace(corner1(1), corner2(1), nMesh);
[xq,yq] = meshgrid(xgv, ygv);
zq = griddata(lons, lats, eles, xq, yq, 'cubic'); % interpolating elevation

% Plots
figure(1)

ax1 = subplot(1,2,1);
plot3(lons,lats,eles);
daspect([1 1 1e3]);

ax2 = subplot(1,2,2);
surfc(xq, yq, zq);
hold on
plot3(city1(2), city1(1), eles(1), 'marker','x')
plot3(city2(2), city2(1), eles(end), 'marker','x')
hold off
xlabel('Longitude'); ylabel('Latitude'); zlabel('Elevation');

linkprop([ax1,ax2], {'CameraPosition','CameraUpVector','DataAspectRatio'});

%% 1b.i Calculate spatial gradient (derived) - akin to "slopiness"
spacingx = abs(city1(2)-city2(2))/nLon;
spacingy = abs(city1(1)-city2(1))/nLat;
[dx,dy] = gradient(zq, spacingx, spacingy);
dxy = sqrt(dx.^2 + dy.^2); % gradient vector lengths

% Plots
figure(2)

ax3 = subplot(1,2,1);
surfc(xq, yq, zq);
daspect([1 1 1e3]);

ax4 = subplot(1,2,2);
surf(xq,yq, dxy);

linkprop([ax3,ax4], {'CameraPosition','CameraUpVector','DataAspectRatio'});

%% 1c.i Estimate discrete cost map

cost_i = dxy; % assume gradient is in dollars already

%% 1d. Overall discrete cost map (Calibration & Weights) - combine i, ii...

% Delphi Method?
Cost_Discrete = cost_i * 1; % only one for now


%% 2. Accumlated cost surface - EXPORT to GRASS GIS
% GRASS: r.in.mat, r.cost

DMS = degrees2dms(23); %??? [lat?, lng?] to dms format

map_data = Cost_Discrete;
map_name = 'ParisMilan';
map_title = 'Paris to Milan';
map_northern_edge = max(city1(1), city2(1)); % assuming northern hemisphere
map_southern_edge = min(city1(1), city2(1));
map_eastern_edge = max(city1(2), city2(2)); % assuming western hemisphere
map_western_edge = min(city1(2), city2(2));
save paris_milan.mat map_* -v4

%% 2. Accumlated cost surface - MATLAB algorithm



%% 3. Least Cost Path (LCP)
% GRASS: r.drain


%% 4. Straightening (e.g. tube costs, curvature g-forces)
% Apply formula: Adj_Cost_Map = i + (( 9 ? i ) / 9 ) * Cost_Map
% where i is the straightening factor 0-no change, 9-perfect line



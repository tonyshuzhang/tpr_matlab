function [ grid_matrix, lats, lons ] = get_grid(coord_start, coord_end, lat_pts, lon_pts)
%  input two coordinates (start, end) each [lat, lon] as grid corners 
%  returns cell array of [lat, lon] pairs
%  returns lat and lon as 1D vectors to be used in API requests
%  Copyright 2017 - Tony S. Zhang (TSZ)

    % handle lack of 3rd/4th input arguments
    if nargin < 4 
        lat_pts = round(abs(coord_start(1) - coord_end(1))) + 1;
        lon_pts = round(abs(coord_start(2) - coord_end(2))) + 1;
    end
    
    % CAREFUL that lat/lon and [x,y] orders are reversed per convention
    xx = linspace(coord_start(2), coord_end(2), lon_pts); % longitudes
    yy = linspace(coord_start(1), coord_end(1), lat_pts); % latitudes
    [x, y] = meshgrid(xx, yy);
    
    grid_matrix = cell(length(yy), length(xx));
    for i = 1:length(yy) % rows
        for j = 1:length(xx) % cols
            grid_matrix{i,j} = [yy(i), xx(j)];
        end
    end
    
    lons = reshape(x', [], 1); % as column vectors
    lats = reshape(y', [], 1);
end

